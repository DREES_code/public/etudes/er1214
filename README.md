# Allocation personnalisée d’autonomie à domicile : la moitié des plans incluent des aides techniques

Ce dossier fournit les programmes permettant de produire les chiffres  et les illustrations contenus dans le texte de l'Études et résultats n° 1214 de la DREES : "Allocation personnalisée d’autonomie à domicile : la moitié des plans incluent des aides techniques". Ce dossier contient notamment des programmes permettant de nettoyer le contenu des plans APA dans les Remontées Individuelles sur l'APA à domicile, à partir de la version disponible au CASD. 

Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/...

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : Remontées individuelles sur les bénéficiaires de l'APA à domicile, version au 06/07/2021. Traitements : Drees.

Date de la dernière exécution des programmes avant publication, et version des logiciels utilisés : Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 3.6.1., le 06/07/2021. 


